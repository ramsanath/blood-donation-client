import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group('Blood Donation', () {
    final loginScreenButtonFinder = find.byValueKey('open_login');
    final sendOtpButtonFinder = find.byValueKey('send_otp_btn');
    final loginPhoneNumberFinder = find.byValueKey('login_phone_number');
    final otpTextFinder = find.byValueKey('otp_text');
    final submitOtpButton = find.byValueKey('submit_otp_button');

    FlutterDriver driver;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('opens login screen', () async {
      await driver.tap(loginScreenButtonFinder);
      await driver.tap(loginPhoneNumberFinder);
      await driver.enterText('9952133952');
      await driver.tap(sendOtpButtonFinder);
      await driver.tap(otpTextFinder);
      await driver.enterText('123456');
      await driver.tap(submitOtpButton);

    });
  });
}
