import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'blocs/auth/bloc.dart';
import 'common/dependency_provider.dart';
import 'common/simple_bloc_delegate.dart';
import 'constants/theme.dart';
import 'generated/i18n.dart';
import 'ui/register/register_screen.dart';
import 'ui/create_request/create_request_screen.dart';
import 'ui/home/home_screen.dart';
import 'ui/login/login_screen.dart';
import 'ui/splash/splash_screen.dart';
import 'ui/welcome/welcome_screen.dart';

void main() async {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(DependencyProvider(child: App()));
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  AuthBloc _authBloc;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      bloc: _authBloc,
      builder: (context, state) {
        return MaterialApp(
          navigatorKey:
              DependencyProvider.of(context).navigationBloc.navigatorKey,
          routes: routes,
          onGenerateTitle: (BuildContext context) => S.of(context).app_name,
          localizationsDelegates: [S.delegate],
          supportedLocales: S.delegate.supportedLocales,
          localeResolutionCallback: S.delegate.resolution(
            fallback: Locale('en', ''),
          ),
          localeListResolutionCallback: S.delegate.listResolution(
            fallback: Locale('en', ''),
          ),
          theme: appTheme(context),
        );
      },
    );
  }

  get routes {
    return {
      '/': (context) => SplashScreen(),
      '/blocs.home': (context) => HomeScreen(),
      '/blocs.login': (context) => LoginScreen(),
      '/ui.welcome': (context) => WelcomeScreen(),
      '/blocs.register': (context) => RegisterScreen(),
      '/create_request': (context) => CreateRequestScreen(),
    };
  }

  @override
  void didChangeDependencies() {
    if (_authBloc == null) {
      _authBloc = DependencyProvider.of(context).authBloc;
      _authBloc.dispatch(AppStartedEvent());
    }
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    DependencyProvider.of(context).dispose();
    super.dispose();
  }
}
