import 'package:blood_donation/models/location.dart';
import 'package:json_annotation/json_annotation.dart';

part 'place.g.dart';

@JsonSerializable()
class Place {
  String name;
  String address;
  Location location = Location();
  String phoneNumber;
  DateTime opensAt;
  DateTime closesAt;
  String gmapsId;

  Place({
    this.name,
    this.address,
    this.location,
    this.phoneNumber,
    this.opensAt,
    this.closesAt,
    this.gmapsId,
  });

  Place copyWith({
    String name,
    String address,
    Location location,
    String phoneNumber,
    DateTime opensAt,
    DateTime closesAt,
    String gmapsId,
  }) {
    return Place(
      name: name ?? this.name,
      address: address ?? this.address,
      location: location ?? this.location,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      opensAt: opensAt ?? this.opensAt,
      closesAt: closesAt ?? this.closesAt,
      gmapsId: gmapsId ?? this.gmapsId,
    );
  }

  factory Place.fromJson(Map<String, dynamic> json) {
    return _$HospitalFromJson(json);
  }

  Map<String, dynamic> toJson() => _$HospitalToJson(this);


}
