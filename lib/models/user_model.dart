import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class User {
  String name;
  String emailAddress;
  String phoneNumber;
  String password;
  String token;

  User({
    this.name,
    this.emailAddress,
    this.phoneNumber,
    this.password,
    this.token,
  });

  factory User.fromJson(Map<String, dynamic> json) =>
      _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  String toString() {
    return 'User{name: $name, emailAddress: $emailAddress, phoneNumber: $phoneNumber, password: $password, token: $token}';
  }
}
