import 'package:json_annotation/json_annotation.dart';

part 'error_data.g.dart';

@JsonSerializable()
class ErrorData {
  int statusCode;
  String message;
  List<String> errors;

  ErrorData({this.statusCode, this.message, this.errors});

  factory ErrorData.fromJson(Map<String, dynamic> json) =>
      _$ErrorDataFromJson(json);

  Map<String, dynamic> toJson() => _$ErrorDataToJson(this);

  @override
  String toString() {
    return 'ErrorData{statusCode: $statusCode, message: $message, errors: $errors}';
  }
}
