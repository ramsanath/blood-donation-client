class SendOtpResponse {
  int expiresIn;

  SendOtpResponse.fromJson(Map<String, dynamic> json) {
    expiresIn = json["expiresIn"];
  }
}