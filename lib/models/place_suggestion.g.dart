// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'place_suggestion.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PlaceSuggestion _$PlaceSuggestionFromJson(Map<String, dynamic> json) {
  return PlaceSuggestion(
    mainText: json['mainText'] as String,
    secondaryText: json['secondaryText'] as String,
    placeId: json['placeId'] as String,
    description: json['description'] as String,
  );
}

Map<String, dynamic> _$PlaceSuggestionToJson(PlaceSuggestion instance) =>
    <String, dynamic>{
      'mainText': instance.mainText,
      'secondaryText': instance.secondaryText,
      'placeId': instance.placeId,
      'description': instance.description,
    };
