import 'package:json_annotation/json_annotation.dart';

part 'login_credentials.g.dart';

@JsonSerializable()
class LoginCredentials {
  String phoneNumber;
  String password;

  LoginCredentials({
    this.phoneNumber,
    this.password,
  });

  @override
  String toString() {
    return 'LoginDto{phoneNumber: $phoneNumber, password: $password}';
  }

  factory LoginCredentials.fromJson(Map<String, dynamic> json) => _$LoginCredentialsFromJson(json);
  Map<String, dynamic> toJson() => _$LoginCredentialsToJson(this);
}
