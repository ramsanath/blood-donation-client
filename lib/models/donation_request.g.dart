// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'donation_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DonationRequest _$DonationRequestFromJson(Map<String, dynamic> json) {
  return DonationRequest(
    patientName: json['patientName'] as String,
    patientGender: json['patientGender'] as String,
    patientBloodGroup: json['patientBloodGroup'] as String,
    unitsRequired: json['unitsRequired'] as int,
    venue: json['venue'] == null
        ? null
        : Place.fromJson(json['venue'] as Map<String, dynamic>),
    requiredOn: json['requiredOn'] == null
        ? null
        : DateTime.parse(json['requiredOn'] as String),
    requiredImmediately: json['requiredImmediately'] as bool,
    description: json['description'] as String,
  );
}

Map<String, dynamic> _$DonationRequestToJson(DonationRequest instance) =>
    <String, dynamic>{
      'patientName': instance.patientName,
      'patientGender': instance.patientGender,
      'patientBloodGroup': instance.patientBloodGroup,
      'unitsRequired': instance.unitsRequired,
      'venue': instance.venue,
      'requiredOn': instance.requiredOn?.toIso8601String(),
      'requiredImmediately': instance.requiredImmediately,
      'description': instance.description,
    };
