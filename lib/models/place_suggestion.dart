import 'package:json_annotation/json_annotation.dart';

part 'place_suggestion.g.dart';

@JsonSerializable()
class PlaceSuggestion {
  String mainText;
  String secondaryText;
  String placeId;
  String description;

  PlaceSuggestion({
    this.mainText,
    this.secondaryText,
    this.placeId,
    this.description,
  });

  factory PlaceSuggestion.fromJson(Map<String, dynamic> json) {
    return _$PlaceSuggestionFromJson(json);
  }

  Map<String, dynamic> toJson() => _$PlaceSuggestionToJson(this);
}
