// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'place.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Place _$HospitalFromJson(Map<String, dynamic> json) {
  return Place(
    name: json['name'] as String,
    address: json['address'] as String,
    location: json['location'] == null
        ? null
        : Location.fromJson(json['location'] as Map<String, dynamic>),
    phoneNumber: json['phoneNumber'] as String,
    opensAt: json['opensAt'] == null
        ? null
        : DateTime.parse(json['opensAt'] as String),
    closesAt: json['closesAt'] == null
        ? null
        : DateTime.parse(json['closesAt'] as String),
    gmapsId: json['gmapsId'] as String,
  );
}

Map<String, dynamic> _$HospitalToJson(Place instance) => <String, dynamic>{
      'name': instance.name,
      'address': instance.address,
      'location': instance.location,
      'phoneNumber': instance.phoneNumber,
      'opensAt': instance.opensAt?.toIso8601String(),
      'closesAt': instance.closesAt?.toIso8601String(),
      'gmapsId': instance.gmapsId,
    };
