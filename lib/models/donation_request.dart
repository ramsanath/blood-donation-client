import 'package:blood_donation/models/place.dart';
import 'package:json_annotation/json_annotation.dart';

part 'donation_request.g.dart';

@JsonSerializable()
class DonationRequest {
  String patientName;
  String patientGender;
  String patientBloodGroup;
  int unitsRequired;
  Place venue = Place();
  DateTime requiredOn;
  bool requiredImmediately;
  String attenderName;
  String attenderPhoneNumber;
  String description;

  DonationRequest({
    this.patientName,
    this.patientGender,
    this.patientBloodGroup,
    this.unitsRequired,
    this.venue,
    this.requiredOn,
    this.requiredImmediately,
    this.attenderPhoneNumber,
    this.attenderName,
    this.description,
  });

  factory DonationRequest.fromJson(Map<String, dynamic> json) {
    return _$DonationRequestFromJson(json);
  }

  Map<String, dynamic> toJson() => _$DonationRequestToJson(this);

  DonationRequest copyWith({
    String patientName,
    String patientGender,
    String patientBloodGroup,
    int unitsRequired,
    Place venue,
    DateTime requiredOn,
    bool requiredImmediately,
    String attenderName,
    String attenderPhoneNumber,
    String description,
  }) {
    return new DonationRequest(
      patientName: patientName ?? this.patientName,
      patientGender: patientGender ?? this.patientGender,
      patientBloodGroup: patientBloodGroup ?? this.patientBloodGroup,
      unitsRequired: unitsRequired ?? this.unitsRequired,
      venue: venue ?? this.venue,
      requiredOn: requiredOn ?? this.requiredOn,
      requiredImmediately: requiredImmediately ?? this.requiredImmediately,
      attenderName: attenderName ?? this.attenderName,
      attenderPhoneNumber: attenderPhoneNumber ?? this.attenderPhoneNumber,
      description: description ?? this.description,
    );
  }
}
