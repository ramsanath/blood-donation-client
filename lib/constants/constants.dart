const bloodGroups = <String>[
  'A+',
  'B+',
  'AB+',
  'O+',
  'A-',
  'B-',
  'AB-',
  'O-',
];

const formItemGap = 30.0;