import 'package:flutter/material.dart';

import 'colors.dart';

ThemeData appTheme(BuildContext context) {
  return ThemeData(
    splashColor: Colors.transparent,
    primarySwatch: AppColor,
    accentColor: AppColor,
    inputDecorationTheme: InputDecorationTheme(
      hasFloatingPlaceholder: false,
      contentPadding: EdgeInsets.symmetric(
        vertical: 15,
        horizontal: 1,
      ),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
      ),
    ),
    buttonTheme: ButtonThemeData(
      textTheme: ButtonTextTheme.primary,
    ),
    textTheme: Theme.of(context)
        .textTheme
        .apply(
          fontFamily: 'OpenSans',
          bodyColor: AppTextColor,
          displayColor: AppTextColor,
        )
        .copyWith(
          title: TextStyle(fontWeight: FontWeight.bold),
        ),
  );
}
