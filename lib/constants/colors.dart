import 'package:flutter/material.dart';

const int _appColorPrimary = 0xFFfc2c4a;
const MaterialColor AppColor = MaterialColor(
  _appColorPrimary,
  <int, Color>{
    50: Color(0xFFfd95a4),
    100: Color(0xFFfd8092),
    200: Color(0xFFfc6b80),
    300: Color(0xFFfc566e),
    400: Color(0xFFfc415c),
    500: Color(_appColorPrimary),
    600: Color(0xFFe22742),
    700: Color(0xFFc9233b),
    800: Color(0xFFb01e33),
    900: Color(0xFF971a2c),
  },
);

const _appTextColor = 0xFF505866;
const AppTextColor = MaterialColor(
  _appTextColor,
  <int, Color>{
    50: Color(0xFFe0e1e4),
    100: Color(0xFFc1c4c9),
    200: Color(0xFFa4a8af),
    300: Color(0xFF878c96),
    400: Color(0xFF6b717e),
    500: Color(_appTextColor),
    600: Color(0xFF434955),
    700: Color(0xFF363b44),
    800: Color(0xFF2a2d34),
    900: Color(0xFF1e2024),
  }
);
