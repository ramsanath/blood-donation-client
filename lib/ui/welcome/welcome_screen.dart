import 'package:blood_donation/ui/widgets/app_button.dart';
import 'package:blood_donation/ui/widgets/header.dart';
import 'package:blood_donation/generated/i18n.dart';
import 'package:blood_donation/ui/login/login_screen.dart';
import 'package:blood_donation/ui/register/register_screen.dart';
import 'package:flutter/material.dart';

class WelcomeScreen extends StatefulWidget {
  WelcomeScreen({Key key}) : super(key: key);

  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Align(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Header(),
                      SizedBox(height: 30),
                      Text(
                        S.of(context).msg_app_info,
                        style: Theme.of(context).textTheme.subhead,
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
              ),
              _renderButtons(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _renderButtons() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        AppButton(
          key: Key('open_login'),
          title: S.of(context).label_login.toUpperCase(),
          onPressed: _onLoginButtonPressed,
          type: "outline",
        ),
        SizedBox(height: 20),
        AppButton(
          title: S.of(context).label_register.toUpperCase(),
          onPressed: _onRegisterButtonPressed,
          type: "flat",
        ),
        SizedBox(height: 50)
      ],
    );
  }

  _onLoginButtonPressed() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => LoginScreen(),
      ),
    );
  }

  _onRegisterButtonPressed() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => RegisterScreen(),
      ),
    );
  }
}
