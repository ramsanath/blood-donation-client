import 'package:flutter/material.dart';

Future<DateTime> showDateTimePicker({BuildContext context}) async {
  final date = await showDatePicker(
    context: context,
    firstDate: DateTime.now(),
    initialDate: DateTime.now(),
    lastDate: DateTime.now().add(Duration(days: 3)),
  );

  if (date == null) return null;

  final time = await showTimePicker(
    context: context,
    initialTime: TimeOfDay(hour: 10, minute: 0),
  );

  if (time == null) return null;

  final dateWithTime = DateTime(
    date.year,
    date.month,
    date.day,
    time.hour,
    time.minute,
  );

  return dateWithTime;
}
