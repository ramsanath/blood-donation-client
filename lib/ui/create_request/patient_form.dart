import 'package:blood_donation/blocs/create_request/bloc.dart';
import 'package:blood_donation/blocs/create_request/create_request_state.dart';
import 'package:blood_donation/common/dismiss_keyboard_view.dart';
import 'package:blood_donation/common/util.dart';
import 'package:blood_donation/common/validators.dart';
import 'package:blood_donation/constants/constants.dart';
import 'package:blood_donation/generated/i18n.dart';
import 'package:blood_donation/ui/widgets/form_title.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:line_icons/line_icons.dart';

class PatientForm extends StatefulWidget {
  final CreateRequestBloc bloc;
  final Validator validator;
  final GlobalKey<FormState> formKey;
  final Key key;

  PatientForm({
    @required this.key,
    this.bloc,
    this.validator,
    this.formKey,
  }) : super(key: key);

  @override
  _PatientFormState createState() => _PatientFormState(bloc);
}

class _PatientFormState extends State<PatientForm>
    with AutomaticKeepAliveClientMixin<PatientForm> {
  final CreateRequestBloc bloc;

  _PatientFormState(this.bloc);

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<CreateRequestBloc, CreateRequestState>(
      bloc: widget.bloc,
      builder: (BuildContext context, CreateRequestState state) {
        return DismissKeyboardView(
          child: Form(
            key: widget.formKey,
            child: ListView(
              padding: EdgeInsets.all(16.0),
              children: <Widget>[
                FormTitle(title: S.of(context).label_patient_details),
                TextFormField(
                  maxLines: 1,
                  keyboardType: TextInputType.text,
                  textCapitalization: TextCapitalization.words,
                  decoration: InputDecoration(
                    prefixIcon: Icon(LineIcons.user),
                    hintText: S.of(context).label_patient_name,
                  ),
                  validator: widget.validator.nameValidator,
                  textInputAction: TextInputAction.next,
                  onSaved: (val) => bloc.dispatch(PatientNameChangeEvent(val)),
                ),
                SizedBox(height: formItemGap),
                _buildGenderSelector(context, state),
                SizedBox(height: formItemGap),
                _buildBloodGroupSelector(context, state),
                SizedBox(height: formItemGap),
                TextFormField(
                  maxLines: 1,
                  keyboardType: TextInputType.numberWithOptions(
                    decimal: false,
                    signed: false,
                  ),
                  decoration: InputDecoration(
                    prefixIcon: Icon(LineIcons.bars),
                    hintText: S.of(context).label_units_required,
                  ),
                  validator: widget.validator.positiveNumberValidator,
                  textInputAction: TextInputAction.next,
                  onSaved: (val) =>
                      bloc.dispatch(UnitsRequiredChangeEvent(parseInt(val))),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildGenderSelector(BuildContext context, CreateRequestState state) {
    final options = <String>[
      S.of(context).label_male,
      S.of(context).label_female,
      S.of(context).label_others,
    ];

    final onChange = (val) => bloc.dispatch(PatientGenderChangeEvent(val));

    return DropdownButtonFormField(
      decoration: InputDecoration(
        hintText: S.of(context).label_patient_gender,
        prefixIcon: Icon(LineIcons.transgender),
      ),
      onSaved: onChange,
      onChanged: onChange,
      value: state.request.patientGender,
      validator: widget.validator.genderValidator,
      items: options
          .map<DropdownMenuItem<String>>(
            (value) => DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            ),
          )
          .toList(),
    );
  }

  Widget _buildBloodGroupSelector(
    BuildContext context,
    CreateRequestState state,
  ) {
    final onChange = (val) => bloc.dispatch(PatientBloodGroupChangeEvent(val));

    return DropdownButtonFormField(
      decoration: InputDecoration(
        hintText: S.of(context).label_patient_blood_group,
        prefixIcon: Icon(LineIcons.tint),
      ),
      value: state.request.patientBloodGroup,
      onSaved: onChange,
      validator: widget.validator.bloodGroupValidator,
      onChanged: onChange,
      items: bloodGroups
          .map<DropdownMenuItem<String>>(
            (value) => DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            ),
          )
          .toList(),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
