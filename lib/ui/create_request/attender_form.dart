import 'package:blood_donation/blocs/create_request/bloc.dart';
import 'package:blood_donation/common/dismiss_keyboard_view.dart';
import 'package:blood_donation/common/validators.dart';
import 'package:blood_donation/constants/constants.dart';
import 'package:blood_donation/generated/i18n.dart';
import 'package:blood_donation/ui/widgets/bloc_screen.dart';
import 'package:blood_donation/ui/widgets/form_title.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class AttenderForm extends StatefulWidget {
  final CreateRequestBloc bloc;
  final Validator validator;
  final GlobalKey<FormState> formKey;

  AttenderForm({
    @required Key key,
    this.bloc,
    this.validator,
    this.formKey,
  }) : super(key: key);

  @override
  _AttenderFormState createState() => _AttenderFormState(bloc);
}

class _AttenderFormState extends State<AttenderForm>
    with AutomaticKeepAliveClientMixin<AttenderForm> {
  final CreateRequestBloc bloc;
  final _nameController = TextEditingController();
  final _phoneNumberController = TextEditingController();

  _AttenderFormState(this.bloc);

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocScreen<CreateRequestBloc, CreateRequestState>(
      bloc: widget.bloc,
      listener: _listener,
      builder: (BuildContext context, CreateRequestState state) {
        final userIsAttender = state.userIsAttender;
        return DismissKeyboardView(
          child: Form(
            key: widget.formKey,
            child: ListView(
              padding: EdgeInsets.all(16.0),
              children: <Widget>[
                FormTitle(title: S.of(context).label_attender_details),
                _buildUserIsAttenderCheckBox(userIsAttender),
                SizedBox(height: formItemGap),
                TextFormField(
                  maxLines: 1,
                  controller: _nameController
                    ..text = state.request.attenderName,
                  enabled: !userIsAttender,
                  keyboardType: TextInputType.text,
                  textCapitalization: TextCapitalization.words,
                  decoration: InputDecoration(
                    prefixIcon: Icon(LineIcons.user),
                    hintText: S.of(context).label_attender_name,
                  ),
                  validator: widget.validator.phoneNumberValidator,
                  textInputAction: TextInputAction.next,
                  onSaved: (val) => bloc.dispatch(AttenderNameChangeEvent(val)),
                ),
                SizedBox(height: formItemGap),
                TextFormField(
                  maxLines: 1,
                  controller: _phoneNumberController
                    ..text = state.request.attenderPhoneNumber,
                  enabled: !userIsAttender,
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                    prefixIcon: Icon(LineIcons.phone),
                    hintText: S.of(context).label_attender_phone_number,
                  ),
                  validator: widget.validator.phoneNumberValidator,
                  textInputAction: TextInputAction.next,
                  onSaved: (val) => bloc.dispatch(AttenderNameChangeEvent(val)),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildUserIsAttenderCheckBox(bool checked) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          S.of(context).label_i_am_attender,
          style: Theme.of(context)
              .textTheme
              .body1
              .copyWith(color: Colors.grey[600], fontSize: 16),
        ),
        Checkbox(
          value: checked,
          activeColor: Theme.of(context).primaryColor,
          onChanged: (value) => bloc.dispatch(UserIsAttenderChangeEvent(value)),
        )
      ],
    );
  }

  _listener(BuildContext context, CreateRequestState state) {
    _nameController.text = state.request.attenderName;
    _phoneNumberController.text = state.request.attenderPhoneNumber;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc.dispatch(UserIsAttenderChangeEvent(true));
  }

  @override
  bool get wantKeepAlive => true;
}
