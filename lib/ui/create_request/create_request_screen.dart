import 'package:blood_donation/blocs/create_request/bloc.dart';
import 'package:blood_donation/common/dependency_provider.dart';
import 'package:blood_donation/common/validators.dart';
import 'package:blood_donation/generated/i18n.dart';
import 'package:blood_donation/ui/create_request/donation_venue_form.dart';
import 'package:blood_donation/ui/create_request/attender_form.dart';
import 'package:blood_donation/ui/widgets/app_button.dart';
import 'package:blood_donation/ui/widgets/bloc_screen.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

import 'donation_time_form.dart';
import 'patient_form.dart';

class CreateRequestScreen extends StatefulWidget {
  @override
  _CreateRequestScreenState createState() => _CreateRequestScreenState();
}

class _CreateRequestScreenState extends State<CreateRequestScreen> {
  CreateRequestBloc _bloc;
  Validator _validator;
  final _patientFormKey = GlobalKey<FormState>();
  final _placeFormKey = GlobalKey<FormState>();
  final _donationTimeKey = GlobalKey<FormState>();
  final _extraFormKey = GlobalKey<FormState>();
  final _formPageController = PageController(keepPage: true);
  int _formCount = 3;
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocScreen<CreateRequestBloc, CreateRequestState>(
        bloc: _bloc,
        listener: _blocListener,
        builder: (BuildContext context, CreateRequestState state) {
          return SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                _buildForm(state),
                _buildSubmitButton(state),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _buildForm(CreateRequestState state) {
    return Expanded(
      flex: 1,
      child: Form(
        child: PageView(
          controller: _formPageController,
          onPageChanged: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
          children: <Widget>[
            PatientForm(
              key: PageStorageKey<String>("patient"),
              bloc: _bloc,
              validator: _validator,
              formKey: _patientFormKey,
            ),
            DonationVenueForm(
              key: PageStorageKey<String>("venue"),
              bloc: _bloc,
              validator: _validator,
              formKey: _placeFormKey,
            ),
            DonationTimeForm(
              bloc: _bloc,
              validator: _validator,
              formKey: _donationTimeKey,
            ),
            AttenderForm(
              key: PageStorageKey<String>("attender"),
              formKey: _extraFormKey,
              bloc: _bloc,
              validator: _validator,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSubmitButton(CreateRequestState state) {
    final onPress = () {
      if (_currentIndex == 0) {
        final valid = _patientFormKey.currentState.validate();
        if (!valid) {
          return;
        }
      } else if (_currentIndex == 1) {
        final valid = _placeFormKey.currentState.validate();
        if (!valid) {
          return;
        }
      } else if (_currentIndex == 2) {
        final valid = _donationTimeKey.currentState.validate();
        if (!valid) {
          return;
        }
      } else if (_currentIndex == 3) {}
      _formPageController.nextPage(
        duration: Duration(milliseconds: 200),
        curve: Curves.decelerate,
      );
    };

    final isLastForm = _currentIndex == _formCount;

    return Padding(
      padding: EdgeInsets.all(20),
      child: Row(
        children: <Widget>[
          AppButton(
            onPressed: () {
              _formPageController.previousPage(
                duration: Duration(milliseconds: 200),
                curve: Curves.decelerate,
              );
            },
            icon: LineIcons.angle_left,
            type: "outline",
          ),
          SizedBox(width: 20),
          Expanded(
            flex: 1,
            child: AppButton(
              onPressed: onPress,
              title: isLastForm ? "SUBMIT" : "NEXT",
              type: isLastForm ? "flat" : "outline",
            ),
          ),
        ],
      ),
    );
  }

  _blocListener(BuildContext context, CreateRequestState state) {}

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _bloc = CreateRequestBloc(
      requestRepo: DependencyProvider.of(context).requestRepo,
      userRepo: DependencyProvider.of(context).userRepo
    );
    _validator = Validator(S.of(context));
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}
