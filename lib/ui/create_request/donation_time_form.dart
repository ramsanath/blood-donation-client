import 'package:blood_donation/blocs/create_request/bloc.dart';
import 'package:blood_donation/blocs/create_request/create_request_bloc.dart';
import 'package:blood_donation/common/validators.dart';
import 'package:blood_donation/constants/constants.dart';
import 'package:blood_donation/generated/i18n.dart';
import 'package:blood_donation/ui/widgets/bloc_screen.dart';
import 'package:blood_donation/ui/widgets/form_title.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:line_icons/line_icons.dart';

import '../date_time_picker.dart';

class DonationTimeForm extends StatefulWidget {
  final CreateRequestBloc bloc;
  final Validator validator;
  final GlobalKey<FormState> formKey;

  DonationTimeForm({
    this.bloc,
    this.validator,
    this.formKey,
  });

  @override
  _DonationTimeFormState createState() => _DonationTimeFormState(
        validator: validator,
        bloc: bloc,
        formKey: formKey,
      );
}

class _DonationTimeFormState extends State<DonationTimeForm>
    with AutomaticKeepAliveClientMixin<DonationTimeForm> {
  final CreateRequestBloc bloc;
  final Validator validator;
  final GlobalKey<FormState> formKey;

  final _requiredOnController = TextEditingController();
  DateTime _requiredOn;

  _DonationTimeFormState({
    this.bloc,
    this.validator,
    this.formKey,
  });

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocScreen(
      bloc: bloc,
      listener: _listener,
      builder: (BuildContext context, CreateRequestState state) {
        return Form(
          key: formKey,
          child: ListView(
            padding: EdgeInsets.all(16.0),
            children: <Widget>[
              FormTitle(title: "Donation Time"),
              TextFormField(
                enableInteractiveSelection: false,
                focusNode: FocusNode(canRequestFocus: false),
                controller: _requiredOnController,
                enabled: !state.request.requiredImmediately,
                maxLines: 1,
                keyboardType: TextInputType.datetime,
                onTap: _showDateTimePicker,
                decoration: InputDecoration(
                  prefixIcon: Icon(LineIcons.calendar),
                  labelText: S.of(context).label_required_on,
                ),
                validator: validator.isNotEmptyValidator,
                textInputAction: TextInputAction.done,
                onSaved: (val) =>
                    bloc.dispatch(RequiredOnChangeEvent(_requiredOn)),
              ),
              SizedBox(height: formItemGap),
              _buildRequiredImmediately(state.request.requiredImmediately),
            ],
          ),
        );
      },
    );
  }

  _showDateTimePicker() async {
    final dateTime = await showDateTimePicker(context: context);
    if (dateTime == null) return;

    setState(() {
      _requiredOn = dateTime;
    });

    _requiredOnController.text = DateFormat("dd-MM-yyyy HH:mm").format(dateTime);
  }

  void _listener(BuildContext context, state) {

  }

  @override
  bool get wantKeepAlive => true;

  Widget _buildRequiredImmediately(bool checked) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          S.of(context).label_required_immediately + "?",
          style: Theme.of(context)
              .textTheme
              .body1
              .copyWith(color: Colors.grey[600], fontSize: 16),
        ),
        Checkbox(
          value: checked,
          activeColor: Theme.of(context).primaryColor,
          onChanged: (checked) {
            bloc.dispatch(RequiredImmediatelyChangeEvent(checked));
          },
        )
      ],
    );
  }
}
