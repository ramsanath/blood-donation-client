import 'package:blood_donation/blocs/create_request/bloc.dart';
import 'package:blood_donation/common/validators.dart';
import 'package:blood_donation/constants/constants.dart';
import 'package:blood_donation/generated/i18n.dart';
import 'package:blood_donation/ui/search_place/search_place_screen.dart';
import 'package:blood_donation/ui/widgets/bloc_screen.dart';
import 'package:blood_donation/ui/widgets/form_title.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class DonationVenueForm extends StatefulWidget {
  final GlobalKey<FormState> formKey;
  final CreateRequestBloc bloc;
  final Validator validator;

  DonationVenueForm({
    @required Key key,
    this.formKey,
    this.bloc,
    this.validator,
  }) : super(key: key);

  @override
  _DonationVenueFormState createState() => _DonationVenueFormState(
        formKey: formKey,
        bloc: bloc,
        validator: validator,
      );
}

class _DonationVenueFormState extends State<DonationVenueForm>
    with AutomaticKeepAliveClientMixin<DonationVenueForm> {
  final CreateRequestBloc bloc;
  final GlobalKey<FormState> formKey;
  final Validator validator;

  Validator _validator;
  TextEditingController _nameController = TextEditingController();
  TextEditingController _addressController = TextEditingController();
  TextEditingController _phoneNumberController = TextEditingController();

  _DonationVenueFormState({this.bloc, this.formKey, this.validator});

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocScreen(
      bloc: bloc,
      listener: _listener,
      builder: (BuildContext context, CreateRequestState state) {
        return Form(
          key: formKey,
          child: ListView(
            padding: EdgeInsets.all(16.0),
            children: <Widget>[
              FormTitle(title: "Donation Venue"),
              TextField(
                maxLines: 1,
                onTap: _searchFieldFocusListener,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  prefixIcon: Icon(LineIcons.search),
                  labelText: "Search",
                ),
                textInputAction: TextInputAction.search,
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0),
                child: Divider(),
              ),
              TextFormField(
                maxLines: 1,
                controller: _nameController,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  prefixIcon: Icon(LineIcons.hospital_o),
                  labelText: S.of(context).label_venue_name,
                ),
                validator: _validator.nameValidator,
                textInputAction: TextInputAction.next,
                onSaved: (val) => bloc.dispatch(VenueNameChangeEvent(val)),
              ),
              SizedBox(height: formItemGap),
              TextFormField(
                maxLines: 3,
                keyboardType: TextInputType.text,
                controller: _addressController,
                decoration: InputDecoration(
                  prefixIcon: Icon(LineIcons.commenting),
                  labelText: S.of(context).label_address,
                ),
                validator: _validator.addressValidator,
                textInputAction: TextInputAction.next,
                onSaved: (val) => bloc.dispatch(VenueAddressChangeEvent(val)),
              ),
              SizedBox(height: formItemGap),
              TextFormField(
                maxLines: 1,
                keyboardType: TextInputType.number,
                controller: _phoneNumberController,
                decoration: InputDecoration(
                  prefixIcon: Icon(LineIcons.phone),
                  labelText: S.of(context).label_venue_phone_number,
                ),
                validator: _validator.phoneNumberValidator,
                textInputAction: TextInputAction.next,
                onSaved: (val) =>
                    bloc.dispatch(VenuePhoneNumberChangeEvent(val)),
              ),
            ],
          ),
        );
      },
    );
  }

  void _searchFieldFocusListener() async {
    final result = await Navigator.of(context).push(
      PageRouteBuilder(
        pageBuilder: (c, a1, a2) => SearchPlaceScreen(),
        transitionsBuilder: (c, anim, a2, child) => FadeTransition(
          opacity: anim,
          child: child,
        ),
        transitionDuration: Duration(milliseconds: 300),
      ),
    );
    bloc.dispatch(VenueChangeEvent(result));
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _validator = Validator(S.of(context));
  }

  void _listener(BuildContext context, CreateRequestState state) {
    final venue = state.request.venue;
    if (state.request.venue != null) {
      _nameController.text = venue.name;
      _addressController.text = venue.address;
      _phoneNumberController.text = venue.phoneNumber;
    }
  }

  @override
  bool get wantKeepAlive => true;
}
