import 'package:blood_donation/blocs/otp/bloc.dart';
import 'package:blood_donation/ui/widgets/app_button.dart';
import 'package:blood_donation/ui/widgets/header.dart';
import 'package:blood_donation/common/dependency_provider.dart';
import 'package:blood_donation/generated/i18n.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'otp_form.dart';

class OtpScreen extends StatefulWidget {
  final String phoneNumber;

  OtpScreen({@required this.phoneNumber});

  @override
  _OtpScreenState createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  OtpBloc _otpBloc;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: BlocBuilder<OtpBloc, OtpState>(
            bloc: _otpBloc,
            builder: (context, state) {
              return _buildContent(state);
            },
          ),
        ),
      ),
    );
  }

  _buildContent(OtpState state) {
    return ListView(
      children: <Widget>[
        Header(),
        Padding(
          padding: EdgeInsets.all(20),
          child: Text(
            "Verify Phone Number",
            style: Theme.of(context).textTheme.title,
            softWrap: true,
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: 30),
        _buildMainContent(state),
      ],
    );
  }

  Widget _buildMainContent(OtpState state) {
    if (state is OtpSendingState) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            S.of(context).msg_sending_otp_to(widget.phoneNumber),
            style: Theme.of(context).textTheme.subhead,
          ),
          SizedBox(height: 20),
          CircularProgressIndicator(),
        ],
      );
    }
    if (state is OtpSentState) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            'Please enter the OTP you received to your mobile number ${widget.phoneNumber}',
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 20),
          OtpForm(onSubmit: returnOtp),
        ],
      );
    }
    if (state is OtpSendFailState) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            S.of(context).msg_err_otp_failed,
            style: Theme.of(context).textTheme.subhead,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              AppButton(
                title: S.of(context).label_retry.toUpperCase(),
                onPressed: () => _otpBloc.sendOtp(widget.phoneNumber),
                type: "outline",
              ),
              SizedBox(width: 20),
              AppButton(
                title: S.of(context).label_cancel.toUpperCase(),
                onPressed: () => returnOtp(null),
                type: "outline",
              )
            ],
          )
        ],
      );
    }
    return Container(
      child: Text("unknown state"),
    );
  }

  returnOtp(String otp) {
    Navigator.of(context).pop(otp);
  }

  @override
  void didChangeDependencies() {
    _otpBloc = OtpBloc(api: DependencyProvider.of(context).api);

    _otpBloc.sendOtp(widget.phoneNumber);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _otpBloc.dispose();
    super.dispose();
  }
}
