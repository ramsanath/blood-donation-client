import 'package:blood_donation/ui/widgets/app_button.dart';
import 'package:blood_donation/generated/i18n.dart';
import 'package:blood_donation/common/validators.dart';
import 'package:flutter/material.dart';

class OtpForm extends StatefulWidget {
  final Function(String) onSubmit;
  final bool loading;
  final String loadingText;

  OtpForm({
    @required this.onSubmit,
    this.loading,
    this.loadingText = "",
  });

  @override
  _OtpFormState createState() => _OtpFormState();
}

class _OtpFormState extends State<OtpForm> {
  final _formKey = GlobalKey<FormState>();
  String _otp;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Material(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            TextFormField(
              key: Key('otp_text'),
              validator: Validator(S.of(context)).otpValidator,
              onSaved: (val) => _otp = val,
              maxLength: 6,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                letterSpacing: 10,
              ),
              decoration: InputDecoration(
                labelText: "OTP",
                labelStyle:
                    TextStyle(letterSpacing: 1, fontWeight: FontWeight.normal),
                icon: Icon(Icons.lock),
              ),
            ),
            SizedBox(height: 30),
            AppButton(
              key: Key('submit_otp_button'),
              type: "flat",
              title: S.of(context).label_submit.toUpperCase(),
              onPressed: _onSubmit,
            ),
          ],
        ),
      ),
    );
  }

  _onSubmit() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      widget.onSubmit(_otp);
    }
  }
}
