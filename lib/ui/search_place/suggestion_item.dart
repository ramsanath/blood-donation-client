import 'package:blood_donation/models/place_suggestion.dart';
import 'package:flutter/material.dart';

class SuggestionItem extends StatelessWidget {
  final PlaceSuggestion suggestion;
  final Function() onTap;
  final _fontSize = 16.0;

  SuggestionItem({
    @required this.suggestion,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final secondaryText = (suggestion.secondaryText != null)
        ? ", ${suggestion.secondaryText}"
        : "";
    return InkWell(
      onTap: onTap,
      child: Column(
        children: <Widget>[
          ListTile(
            title: RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                    text: suggestion.mainText,
                    style: TextStyle(
                      fontSize: _fontSize,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey[700],
                    ),
                  ),
                  TextSpan(
                    text: secondaryText,
                    style: TextStyle(
                      fontSize: _fontSize,
                      color: Colors.grey[700],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Divider(),
        ],
      ),
    );
  }
}
