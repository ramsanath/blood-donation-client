import 'package:blood_donation/blocs/search_place/bloc.dart';
import 'package:blood_donation/common/debouncer.dart';
import 'package:blood_donation/common/dependency_provider.dart';
import 'package:blood_donation/common/dismiss_keyboard_view.dart';
import 'package:blood_donation/models/place.dart';
import 'package:blood_donation/models/place_suggestion.dart';
import 'package:blood_donation/ui/search_place/suggestion_item.dart';
import 'package:blood_donation/ui/widgets/bloc_screen.dart';
import 'package:blood_donation/ui/widgets/loading_container.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class SearchPlaceScreen extends StatefulWidget {
  @override
  _SearchPlaceScreenState createState() => _SearchPlaceScreenState();
}

class _SearchPlaceScreenState extends State<SearchPlaceScreen> {
  SearchPlaceBloc _bloc;
  final _debouncer = Debouncer(milliseconds: 500);

  @override
  Widget build(BuildContext context) {
    return DismissKeyboardView(
      child: Scaffold(
        body: BlocScreen<SearchPlaceBloc, SearchPlaceState>(
          bloc: _bloc,
          listener: _listener,
          builder: (BuildContext context, SearchPlaceState state) {
            return SafeArea(
              child: LoadingContainer(
                loaderHeight: 5,
                showOverlay: state.loadingDetail,
                loading: state.loading,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: <Widget>[
                      _buildSearchResult(state.suggestions),
                      Image.asset('assets/images/powered_by_google_color.png'),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: _buildSearchBar(),
          automaticallyImplyLeading: false,
          actions: <Widget>[_buildCancelButton()],
        ),
      ),
    );
  }

  Widget _buildSearchResult(List<PlaceSuggestion> suggestions) {
    return Expanded(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: suggestions.length,
        itemBuilder: (BuildContext context, int index) {
          final prediction = suggestions[index];
          return SuggestionItem(
            suggestion: prediction,
            onTap: () => _bloc.getPlaceDetail(prediction),
          );
        },
      ),
    );
  }

  Widget _buildSearchBar() {
    return TextField(
      autofocus: true,
      style: TextStyle(fontWeight: FontWeight.w600),
      decoration: InputDecoration(
        prefixIcon: Icon(LineIcons.search),
        hintText: "Search for Hospital, Location",
        hasFloatingPlaceholder: false,
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
        ),
      ),
      onChanged: (String value) {
        _debouncer.run(() => _bloc.search(value));
      },
    );
  }

  Widget _buildCancelButton() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: AspectRatio(
        aspectRatio: 1.0,
        child: InkWell(
          borderRadius: BorderRadius.circular(100),
          onTap: () => _returnValue(null),
          child: Icon(LineIcons.times, color: Theme.of(context).primaryColor),
        ),
      ),
    );
  }

  _returnValue(Place result) {
    Navigator.of(context).pop(result);
  }

  @override
  void didChangeDependencies() {
    _bloc =
        SearchPlaceBloc(placeRepo: DependencyProvider.of(context).placeRepo);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  _listener(BuildContext context, SearchPlaceState state) {
    if (state.error) {
      Scaffold.of(context)
        ..hideCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            backgroundColor: Colors.red,
            content: Text("Something went wrong. Please try again"),
          ),
        );
    }
    if (state.place != null) {
      _returnValue(state.place);
    }
  }
}
