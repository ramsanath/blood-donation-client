import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Image(
          height: 100,
          width: 100,
          fit: BoxFit.contain,
          image: AssetImage('assets/images/rss-logo.png'),
        ),
        SizedBox(height: 10),
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: 'RSS HSS ',
            style: Theme.of(context)
                .textTheme
                .headline
                .apply(color: primaryColor, fontWeightDelta: 10),
            children: <TextSpan>[
              TextSpan(
                text: ' Blood Donors Bureau',
                style: Theme.of(context)
                    .textTheme
                    .headline
                    .apply(color: primaryColor, fontWeightDelta: 2),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
