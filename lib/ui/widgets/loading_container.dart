import 'package:flutter/material.dart';

class LoadingContainer extends StatelessWidget {
  final Widget child;
  final bool loading;
  final bool showOverlay;
  final Color loaderColor;
  final double loaderHeight;
  final double loaderValue;
  final Color overlayColor;

  LoadingContainer({
    @required this.child,
    this.loading,
    this.showOverlay = true,
    this.loaderColor,
    this.loaderValue,
    this.loaderHeight,
    this.overlayColor = const Color(0x50ffffff),
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[child, _buildOverlay()],
    );
  }

  Widget _buildOverlay() {
    if (!loading) {
      return Container();
    }

    return Column(
      children: <Widget>[
        Container(
          height: loaderHeight,
          child: LinearProgressIndicator(
            backgroundColor: Colors.transparent,
            value: loaderValue,
          ),
        ),
        showOverlay
            ? Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: overlayColor,
                  ),
                  constraints: BoxConstraints.expand(),
                ),
              )
            : Container(),
      ],
    );
  }
}
