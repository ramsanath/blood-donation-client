import 'package:flutter/material.dart';

class AppButton extends StatefulWidget {
  final Function onPressed;
  final String title;
  final String type;
  final Color color;
  final IconData icon;
  final EdgeInsets padding;
  final double width;
  final double height;

  AppButton({
    this.onPressed,
    this.title,
    this.color,
    this.icon,
    this.padding,
    this.width,
    this.height,
    @required this.type,
    Key key,
  })  : assert(icon != null || title != null),
        assert(
          !(icon != null && title != null),
          "Should provide either title or icon",
        ),
        super(key: key);

  @override
  _AppButtonState createState() => _AppButtonState();
}

class _AppButtonState extends State<AppButton> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      width: widget.width,
      child: _buildButton(),
    );
  }

  Widget _buildButton() {
    final _padding = widget.padding ?? EdgeInsets.symmetric(vertical: 20);
    final _shape = RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(1000.0),
    );

    if (widget.type == "outline") {
      return OutlineButton(
        onPressed: widget.onPressed,
        child: _buildChild(),
        borderSide: BorderSide(
          color: widget.color ?? Theme.of(context).primaryColor,
          width: 2,
        ),
        padding: _padding,
        shape: _shape,
        color: Theme.of(context).primaryColor,
      );
    } else {
      return FlatButton(
        onPressed: widget.onPressed,
        child: _buildChild(),
        padding: _padding,
        shape: _shape,
        color: Theme.of(context).primaryColor,
      );
    }
  }

  _buildChild() {
    if (widget.icon != null) {
      if (widget.type == "flat") {
        return Icon(
          widget.icon,
          color: Colors.white,
        );
      } else {
        return Icon(
          widget.icon,
          color: widget.color ?? Theme.of(context).primaryColor,
        );
      }
    }

    final flatStyle = Theme.of(context).textTheme.body1.copyWith(
          color: Colors.white,
          letterSpacing: 2,
          fontWeight: FontWeight.w700,
        );
    final outlineStyle = Theme.of(context).textTheme.body1.copyWith(
          color: widget.color ?? Theme.of(context).primaryColor,
          letterSpacing: 2,
          fontWeight: FontWeight.w700,
        );

    if (widget.type == "flat") {
      return Text(
        widget.title,
        style: flatStyle,
      );
    } else {
      return Text(
        widget.title,
        style: outlineStyle,
      );
    }
  }
}
