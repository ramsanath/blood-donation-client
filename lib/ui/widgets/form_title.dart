import 'package:flutter/material.dart';

class FormTitle extends StatelessWidget {
  final String title;

  FormTitle({this.title});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 24, top: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: Theme.of(context)
                .textTheme
                .title
                .copyWith(color: Theme.of(context).primaryColor),
          ),
          Divider(),
        ],
      ),
    );
  }
}
