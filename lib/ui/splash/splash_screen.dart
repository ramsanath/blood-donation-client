import 'package:blood_donation/blocs/auth/bloc.dart';
import 'package:blood_donation/common/dependency_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AuthBloc _authBloc;

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _authBloc,
      listener: _authStateListener,
      child: Scaffold(
        body: Center(
          child: Text('Splash Screen'),
        ),
      ),
    );
  }

  _authStateListener(BuildContext context, AuthState state) {
    if (state is AuthAuthenticatedState) {
      Navigator.of(context).pushReplacementNamed('/blocs.home');
    } else {
      Navigator.of(context).pushReplacementNamed('/ui.welcome');
    }
  }

  @override
  void didChangeDependencies() {
    _authBloc = DependencyProvider.of(context).authBloc;
    super.didChangeDependencies();
  }
}
