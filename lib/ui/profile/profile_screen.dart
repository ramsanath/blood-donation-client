import 'package:blood_donation/blocs/profile/bloc.dart';
import 'package:blood_donation/common/dependency_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  ProfileBloc _profileBloc;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<ProfileBloc, ProfileState>(
        bloc: _profileBloc,
        listener: _blocListener,
        child: BlocBuilder<ProfileBloc, ProfileState>(
          bloc: _profileBloc,
          builder: (context, state) {
            return Scaffold(
              body: Center(
                child: Center(
                  child: FlatButton(
                    child: Text("Logout"),
                    onPressed: () {
                      DependencyProvider.of(context).authBloc.logout();
                    },
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  _blocListener(BuildContext context, ProfileState state) {}

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _profileBloc = ProfileBloc();
  }

  @override
  void dispose() {
    _profileBloc.dispose();
    super.dispose();
  }
}
