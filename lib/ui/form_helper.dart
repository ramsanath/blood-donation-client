import 'package:flutter/cupertino.dart';

Function(String) getFocusNextHandler(
  BuildContext context,
  FocusNode nextFocus,
) {
  return (val) {
    FocusScope.of(context).requestFocus(nextFocus);
  };
}
