import 'package:blood_donation/blocs/navigation/bloc.dart';
import 'package:blood_donation/common/dependency_provider.dart';
import 'package:blood_donation/ui/widgets/app_button.dart';
import 'package:blood_donation/ui/widgets/bloc_screen.dart';
import 'package:flutter/material.dart';

import '../../blocs/request/bloc.dart';

class RequestScreen extends StatefulWidget {
  @override
  _RequestScreenState createState() => _RequestScreenState();
}

class _RequestScreenState extends State<RequestScreen> {
  RequestBloc _requestBloc;

  NavigationBloc get _navBloc => DependencyProvider.of(context).navigationBloc;

  @override
  Widget build(BuildContext context) {
    return BlocScreen<RequestBloc, RequestState>(
      bloc: _requestBloc,
      listener: _stateListener,
      builder: (BuildContext context, RequestState state) {
        return Scaffold(
          body: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                _buildBody(context, state),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody(BuildContext context, RequestState state) {
    return Center(
      child: AppButton(
        title: "Request".toUpperCase(),
        onPressed: () => _navBloc.goToCreateRequest(),
        type: "flat",
      ),
    );
  }

  _stateListener(BuildContext context, RequestState state) {}

  @override
  void didChangeDependencies() {
    _requestBloc = RequestBloc();
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _requestBloc.dispose();
    super.dispose();
  }
}
