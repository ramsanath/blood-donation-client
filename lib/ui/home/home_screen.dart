import 'package:blood_donation/blocs/home/bloc.dart';
import 'package:blood_donation/ui/profile/profile_screen.dart';
import 'package:blood_donation/ui/request/request_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:line_icons/line_icons.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HomeBloc _homeBloc;
  int _currentIndex = 0;
  final List<Widget> _children = [
    Container(),
    RequestScreen(),
    ProfileScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _buildBottomNavigationBar(),
      body: BlocListener<HomeBloc, HomeState>(
        bloc: _homeBloc,
        listener: _blocListener,
        child: BlocBuilder<HomeBloc, HomeState>(
          bloc: _homeBloc,
          builder: (BuildContext context, HomeState state) {
            return _currentItem;
          },
        ),
      ),
    );
  }

  Widget get _currentItem {
    return _children[_currentIndex];
  }

  Widget _buildBottomNavigationBar() {
    return BottomNavigationBar(
      currentIndex: _currentIndex,
      onTap: _onIndexChange,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(LineIcons.plus),
          title: Text('Donate'),
        ),
        BottomNavigationBarItem(
          icon: Icon(LineIcons.tint),
          title: Text('Request'),
        ),
        BottomNavigationBarItem(
          icon: Icon(LineIcons.user),
          title: Text('Account'),
        )
      ],
    );
  }

  _onIndexChange(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  void _blocListener(BuildContext context, HomeState state) {}

  @override
  void didChangeDependencies() {
    _homeBloc = HomeBloc();
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _homeBloc.dispose();
    super.dispose();
  }
}
