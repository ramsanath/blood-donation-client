import 'package:blood_donation/common/validators.dart';
import 'package:blood_donation/generated/i18n.dart';
import 'package:blood_donation/models/user_model.dart';
import 'package:blood_donation/ui/widgets/app_button.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class RegisterForm extends StatefulWidget {
  final Function(User data) onSubmit;

  RegisterForm({this.onSubmit});

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final _itemPadding = 20.0;
  final _formKey = GlobalKey<FormState>();
  bool _autovalidate = false;
  Validator _formValidator;
  User _user = User();

  @override
  Widget build(BuildContext context) {
    return Form(
      autovalidate: _autovalidate,
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          TextFormField(
            validator: _formValidator.nameValidator,
            onSaved: (val) => _user.name = val,
            keyboardType: TextInputType.text,
            textCapitalization: TextCapitalization.words,
            decoration: InputDecoration(
              prefixIcon: Icon(LineIcons.user),
              hintText: S.of(context).label_name,
            ),
          ),
          SizedBox(height: _itemPadding),
          TextFormField(
            validator: _formValidator.emailAddressValidator,
            onSaved: (val) => _user.emailAddress = val,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              prefixIcon: Icon(LineIcons.at),
              hintText: S.of(context).label_email_address,
            ),
          ),
          SizedBox(height: _itemPadding),
          TextFormField(
            validator: _formValidator.indianPhoneNumberValidator,
            onSaved: (val) => _user.phoneNumber = val,
            keyboardType: TextInputType.phone,
            maxLength: 10,
            decoration: InputDecoration(
              prefixIcon: Icon(LineIcons.phone),
              hintText: S.of(context).label_phone_number,
              prefixText: '+91 ',
            ),
          ),
          TextFormField(
            key: Key('register_password'),
            keyboardType: TextInputType.visiblePassword,
            obscureText: true,
            decoration: InputDecoration(
              prefixIcon: Icon(LineIcons.key),
              hintText: S.of(context).label_password,
            ),
            onSaved: (val) => _user.password = val,
            validator: _formValidator.passwordValidator,
          ),
          SizedBox(height: _itemPadding),
          _buildTncText(),
          AppButton(
            title: S.of(context).label_register.toUpperCase(),
            onPressed: _onSubmit,
            type: "flat",
          )
        ],
      ),
    );
  }

  _buildTncText() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Text.rich(
        TextSpan(
          text: S.of(context).msg_tnc_consent_first,
          style: Theme.of(context).textTheme.body1,
          children: <TextSpan>[
            TextSpan(
              text: S.of(context).msg_tnc_consent_second,
              style: TextStyle(
                decoration: TextDecoration.underline,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void didChangeDependencies() {
    _formValidator = Validator(S.of(context));
    super.didChangeDependencies();
  }

  void _onSubmit() async {
    if (!_formKey.currentState.validate()) {
      setState(() => _autovalidate = true);
      return;
    }
    _formKey.currentState.save();
    widget.onSubmit(_user);
  }
}
