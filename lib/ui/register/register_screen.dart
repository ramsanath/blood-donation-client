import 'package:blood_donation/blocs/register/bloc.dart';
import 'package:blood_donation/common/dependency_provider.dart';
import 'package:blood_donation/models/user_model.dart';
import 'package:blood_donation/ui/widgets/header.dart';
import 'package:blood_donation/ui/widgets/loading_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'register_form.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  RegisterBloc _registerBloc;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<RegisterBloc, RegisterState>(
        bloc: _registerBloc,
        listener: _eventListener,
        child: BlocBuilder(
          bloc: _registerBloc,
          builder: (context, RegisterState state) {
            return SafeArea(
              child: LoadingContainer(
                loading: state.isLoading,
                loaderHeight: 5,
                loaderColor: Theme.of(context).primaryColor,
                child: ListView(
                  padding: EdgeInsets.all(20),
                  children: <Widget>[
                    Header(),
                    SizedBox(height: 30),
                    RegisterForm(onSubmit: _onSubmit),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  void _eventListener(BuildContext context, RegisterState state) {
    if (state.error != null) {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text(state.error),
          backgroundColor: Colors.red,
          duration: Duration(seconds: 5),
        ),
      );
    }
    if (state.isLoading) {}
  }

  @override
  void didChangeDependencies() {
    _registerBloc = RegisterBloc(
      userRepo: DependencyProvider.of(context).userRepo,
      authBloc: DependencyProvider.of(context).authBloc,
    );
    super.didChangeDependencies();
  }

  void _onSubmit(User data) async {
    _registerBloc.register(data);
  }

  @override
  void dispose() {
    _registerBloc.dispose();
    super.dispose();
  }
}
