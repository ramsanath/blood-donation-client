import 'package:blood_donation/common/validators.dart';
import 'package:blood_donation/generated/i18n.dart';
import 'package:blood_donation/models/login_credentials.dart';
import 'package:blood_donation/ui/widgets/app_button.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class LoginForm extends StatefulWidget {
  final Function(LoginCredentials) onSubmit;

  LoginForm({@required this.onSubmit});

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  LoginCredentials _loginDto = LoginCredentials();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          TextFormField(
            key: Key('login_phone_number'),
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              hintText: S.of(context).label_phone_number,
              prefixText: '+91 ',
              prefixIcon: Icon(LineIcons.phone),
            ),
            maxLength: 10,
            validator: Validator(S.of(context)).indianPhoneNumberValidator,
            onSaved: (val) => _loginDto.phoneNumber = val,
          ),
          TextFormField(
            key: Key('login_password'),
            keyboardType: TextInputType.visiblePassword,
            obscureText: true,
            decoration: InputDecoration(
              hintText: S.of(context).label_password,
              prefixIcon: Icon(LineIcons.key),
            ),
            onSaved: (val) => _loginDto.password = val,
            validator: Validator(S.of(context)).passwordValidator,
          ),
          SizedBox(height: 30),
          AppButton(
            key: Key('login_submit_btn'),
            type: "flat",
            title: S.of(context).label_login.toUpperCase(),
            onPressed: _onSubmit,
          )
        ],
      ),
    );
  }

  _onSubmit() async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
    widget.onSubmit(_loginDto);
  }
}
