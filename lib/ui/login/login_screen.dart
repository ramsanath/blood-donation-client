import 'package:blood_donation/blocs/login/bloc.dart';
import 'package:blood_donation/models/login_credentials.dart';
import 'package:blood_donation/ui/widgets/header.dart';
import 'package:blood_donation/ui/widgets/loading_container.dart';
import 'package:blood_donation/common/dependency_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'login_form.dart';


class LoginScreen extends StatefulWidget {
  @override
  State createState() {
    return _LoginScreenState();
  }
}

class _LoginScreenState extends State<LoginScreen> {
  LoginBloc _loginBloc;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<LoginBloc, LoginState>(
        bloc: _loginBloc,
        listener: _loginStateListener,
        child: BlocBuilder<LoginBloc, LoginState>(
          bloc: _loginBloc,
          builder: (BuildContext context, LoginState state) {
            return SafeArea(
              child: LoadingContainer(
                loading: state is LoginLoadingState,
                loaderHeight: 5,
                loaderColor: Theme.of(context).primaryColor,
                child: ListView(
                  padding: EdgeInsets.all(20),
                  children: <Widget>[
                    Header(),
                    SizedBox(height: 30),
                    LoginForm(onSubmit: _onSubmit),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  _loginStateListener(context, LoginState state) {
    if (state is LoginErrorState) {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text(state.error),
          backgroundColor: Colors.red,
          duration: Duration(seconds: 4),
        ),
      );
    }
  }

  void _onSubmit(LoginCredentials data) async {
    _loginBloc.login(data);
  }

  @override
  void didChangeDependencies() {
    _loginBloc = LoginBloc(
      userRepo: DependencyProvider.of(context).userRepo,
      authBloc: DependencyProvider.of(context).authBloc,
    );
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _loginBloc.dispose();
    super.dispose();
  }
}
