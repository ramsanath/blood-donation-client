import 'package:blood_donation/common/util.dart';
import 'package:blood_donation/models/error_data.dart';
import 'package:blood_donation/models/place.dart';
import 'package:blood_donation/models/place_suggestion.dart';
import 'package:blood_donation/models/login_credentials.dart';
import 'package:blood_donation/models/user_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class Api {
  static Api _api;
  Dio _dio;
  final _baseUrlProd = "http://localhost:8000/api/v1"; // TODO CHANGE
  final _loginUrl = "/login";
  final _registerUrl = "/register";
  final _sendOtpUrl = "/send_otp";
  final _searchPlaceUrl = "/search_places";
  final _placeDetailsUrl = "/place_detail";

  Api._() {
    _dio = Dio();
//    _dio.interceptors.add(LogInterceptor(responseBody: true));
    if (kDebugMode) {
      platformValue(
        androidSimulator: "http://10.0.2.2:8000/api/v1/",
        android: "http://192.168.29.84:8000/api/v1/",
        ios: "http://localhost:8000/api/v1",
      ).then((val) => _dio.options.baseUrl = val);
    } else {
      _dio.options.baseUrl = _baseUrlProd;
    }
  }

  factory Api() {
    if (_api == null) {
      _api = Api._();
    }
    return _api;
  }

  Future sendOtp(String phoneNumber) async {
    await makeRequest(
      _sendOtpUrl,
      method: "post",
      body: {"phoneNumber": phoneNumber},
    );
  }

  Future<User> login(LoginCredentials loginDto) async {
    final response = await makeRequest(
      _loginUrl,
      method: "post",
      body: loginDto,
    );
    return User.fromJson(response.data);
  }

  Future<User> register(User user) async {
    final response = await makeRequest(
      _registerUrl,
      method: "post",
      body: user,
    );
    return User.fromJson(response.data);
  }

  Future<List<PlaceSuggestion>> searchPlaces({
    @required String input,
    String location,
    @required String token,
  }) async {
    final response = await makeAuthRequest(
      _searchPlaceUrl,
      method: "get",
      queryParameters: {
        "input": input,
        "location": location,
      },
      token: token,
    );

    return (response.data as List)
        .map((json) => PlaceSuggestion.fromJson(json as Map<String, dynamic>))
        .toList();
  }

  Future<Place> placeDetails({
    @required String googleMapsPlaceId,
    @required String token,
  }) async {
    final response = await makeAuthRequest(
      _placeDetailsUrl,
      method: "get",
      queryParameters: {
        "place_id": googleMapsPlaceId,
      },
      token: token,
    );
    return Place.fromJson(response.data as Map<String, dynamic>);
  }

  Future<Response<dynamic>> makeRequest(
    String url, {
    String method,
    Map<String, dynamic> queryParameters,
    body,
    Map<String, dynamic> headers,
  }) async {
    try {
      return await _dio.request(
        url,
        data: body,
        queryParameters: queryParameters,
        options: Options(
          headers: headers,
          method: method,
        ),
      );
    } on DioError catch (e) {
      if (e.response != null) {
        try {
          var errorData = ErrorData.fromJson(e.response.data);
          errorData.statusCode = e.response.statusCode;
          throw errorData;
        } on Error {
          throw new Error();
        }
      } else {
        throw ErrorData(message: "Connection error");
      }
    }
  }

  Future<Response<dynamic>> makeAuthRequest(
    String url, {
    String method,
    Map<String, dynamic> queryParameters,
    body,
    Map<String, dynamic> headers,
    @required String token,
  }) {
    headers = headers ?? {};
    headers["Authorization"] = token;
    return makeRequest(
      url,
      method: method,
      headers: headers,
      body: body,
      queryParameters: queryParameters,
    );
  }
}
