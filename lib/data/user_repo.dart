import 'package:blood_donation/common/local_storage.dart';
import 'package:blood_donation/data/api.dart';
import 'package:blood_donation/models/user_model.dart';

class UserRepo {
  User currentUser;
  final _userInfoKey = "user-info";
  final LocalStorage localStorage;
  final Api api;

  UserRepo({this.api, this.localStorage});

  Future<User> login(loginDto) async {
    try {
      User response = await api.login(loginDto);
      return response;
    } catch (e) {
      throw e.message;
    }
  }

  Future<User> register(User user) async {
    try {
      User response = await api.register(user);
      return response;
    } catch (e) {
      throw e.message;
    }
  }

  Future<void> deleteToken() async {
    await localStorage.delete(_userInfoKey);
  }

  Future<void> persistUserInfo(User user) async {
    currentUser = user;
    await localStorage.write(_userInfoKey, user.toJson());
  }

  Future<bool> isLoggedIn() async {
    final userInfo = await localStorage.read(_userInfoKey);
    if (userInfo == null) {
      return false;
    } else {
      currentUser = User.fromJson(userInfo);
      return true;
    }
  }
}
