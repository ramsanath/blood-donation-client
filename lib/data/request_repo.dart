import 'package:blood_donation/common/local_storage.dart';
import 'package:blood_donation/data/api.dart';
import 'package:blood_donation/data/user_repo.dart';
import 'package:blood_donation/models/place.dart';
import 'package:blood_donation/models/place_suggestion.dart';
import 'package:flutter/cupertino.dart';

class RequestRepo {
  final LocalStorage localStorage;
  final Api api;
  final UserRepo userRepo;

  RequestRepo({
    @required this.localStorage,
    @required this.api,
    @required this.userRepo,
  });

  Future<List<PlaceSuggestion>> searchHospitals(
    String input,
    String location,
  ) async {
    return await api.searchPlaces(
      input: input,
      location: location,
      token: userRepo.currentUser.token,
    );
  }

  Future<Place> hospitalDetails(String googleMapsPlaceId) async {
    return await api.placeDetails(
      googleMapsPlaceId: googleMapsPlaceId,
      token: userRepo.currentUser.token,
    );
  }
}
