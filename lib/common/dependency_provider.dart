import 'package:blood_donation/blocs/auth/bloc.dart';
import 'package:blood_donation/blocs/navigation/bloc.dart';
import 'package:blood_donation/common/local_storage.dart';
import 'package:blood_donation/data/api.dart';
import 'package:blood_donation/data/hospital_repo.dart';
import 'package:blood_donation/data/request_repo.dart';
import 'package:blood_donation/data/user_repo.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class DependencyProvider extends InheritedWidget {
  Api _api;
  UserRepo _userRepo;
  RequestRepo _requestRepo;
  AuthBloc _authBloc;
  NavigationBloc _navigationBloc;
  LocalStorage _localStorage;
  PlaceRepo _placeRepo;

  Api get api {
    if (_api == null) {
      _api = Api();
    }
    return _api;
  }

  LocalStorage get localStorage {
    if (_localStorage == null) {
      _localStorage = LocalStorage();
    }
    return _localStorage;
  }

  UserRepo get userRepo {
    if (_userRepo == null) {
      _userRepo = UserRepo(
        api: api,
        localStorage: localStorage,
      );
    }
    return _userRepo;
  }

  AuthBloc get authBloc {
    if (_authBloc == null) {
      _authBloc = AuthBloc(
        userRepo: userRepo,
        navigationBloc: navigationBloc,
      );
    }
    return _authBloc;
  }

  NavigationBloc get navigationBloc {
    if (_navigationBloc == null) {
      _navigationBloc = NavigationBloc();
    }
    return _navigationBloc;
  }

  RequestRepo get requestRepo {
    if (_requestRepo == null) {
      _requestRepo = RequestRepo(
        api: api,
        localStorage: localStorage,
        userRepo: userRepo,
      );
    }
    return _requestRepo;
  }

  PlaceRepo get placeRepo {
    if (_placeRepo == null) {
      _placeRepo = PlaceRepo(
        userRepo: userRepo,
        localStorage: localStorage,
        api: api,
      );
    }
    return _placeRepo;
  }

  DependencyProvider({Key key, @required Widget child})
      : assert(child != null),
        super(key: key, child: child);

  static DependencyProvider of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(DependencyProvider)
        as DependencyProvider;
  }

  dispose() {
    _authBloc?.dispose();
    _navigationBloc?.dispose();
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}
