import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class LocalStorage {
  static LocalStorage _singleton =
  LocalStorage._internal(FlutterSecureStorage());
  final FlutterSecureStorage _storage;

  factory LocalStorage() {
    return _singleton;
  }

  LocalStorage._internal(this._storage);

  write(String key, Map<String, dynamic> value) {
    final raw = json.encode(value);
    return _storage.write(key: key, value: raw);
  }

  read(String key) async {
    final raw = await _storage.read(key: key);
    if (raw == null) return null;
    final value = json.decode(raw);
    return value;
  }

  delete(String key) {
    return _storage.delete(key: key);
  }
}
