import 'package:blood_donation/common/util.dart';
import 'package:blood_donation/constants/constants.dart';
import 'package:blood_donation/generated/i18n.dart';

class Validator {
  final S s;

  Validator(this.s);

  String nameValidator(val) {
    if (isFalsy(val)) {
      return s.msg_err_invalid_value(s.label_name);
    }
    val = val.trim();
    if (!RegExp('[a-zA-Z]{3,20}').hasMatch(val)) {
      return "Name should be 3 to 20 characters in length";
    }
    return null;
  }

  String emailAddressValidator(String val) {
    if (isFalsy(val)) {
      return s.msg_err_invalid_value(s.label_email_address);
    }
    val = val.trim();
    if (!validateEmail(val)) {
      return s.msg_err_invalid_value(s.label_email_address);
    }
    return null;
  }

  String indianPhoneNumberValidator(String val) {
    if (isFalsy(val)) {
      return s.msg_err_invalid_value(s.label_phone_number);
    }
    val = val.trim();
    var indianNumberPattern = RegExp('^[6-9]{1}[0-9]{9}');
    if (!indianNumberPattern.hasMatch(val)) {
      return s.msg_err_invalid_value(s.label_phone_number);
    }
    return null;
  }

  String otpValidator(String val) {
    if (!RegExp("^[0-9]{6}").hasMatch(val)) {
      return s.msg_err_invalid_otp;
    }
    return null;
  }

  String passwordValidator(String val) {
    if (val.length < 8) {
      return s.label_too_short(s.label_password);
    }
    if (val.length > 100) {
      return s.label_too_long(s.label_password);
    }
    return null;
  }

  String genderValidator(String val) {
    if (val == null || val.isEmpty) {
      return s.msg_err_empty_value(s.label_patient_gender);
    }
    final genders = [
      s.label_male.toLowerCase(),
      s.label_female.toLowerCase(),
      s.label_others.toLowerCase(),
    ];
    if (!genders.contains(val.toLowerCase())) {
      return s.msg_err_invalid_value(s.label_patient_gender);
    }
    return null;
  }

  String bloodGroupValidator(String val) {
    if (val == null || val.isEmpty) {
      return s.msg_err_empty_value(s.label_patient_blood_group);
    }
    if (!bloodGroups.contains(val)) {
      return s.msg_err_invalid_value(s.label_patient_blood_group);
    }
    return null;
  }

  String positiveNumberValidator(String val) {
    if (val == null || val.isEmpty) {
      return s.msg_err_empty_value(s.label_number);
    }
    int number = parseInt(val);
    if (number == null) {
      return s.msg_err_invalid_value(s.label_number);
    }
    return null;
  }

  String phoneNumberValidator(String val) {
    if (val == null || val.length < 6 || val.length > 20) {
      // TODO: make a valid check
      return s.msg_err_invalid_value(s.label_phone_number);
    }
    return null;
  }

  String addressValidator(String val) {
    if (val == null || val.length < 5) {
      return s.msg_err_empty_value(s.label_address);
    }
    return null;
  }

  String isNotEmptyValidator(String val) {
    if (val == null || val.isEmpty) {
      return s.msg_err_should_not_be_empty;
    }
    return null;
  }
}
