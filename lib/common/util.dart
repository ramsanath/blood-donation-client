import 'dart:io';

import 'package:device_info/device_info.dart';

bool isNumeric(String val) {
  if (val == null) {
    return false;
  }
  try {
    return double.parse(val) != null;
  } catch (e) {
    return false;
  }
}

bool validateEmail(String value) {
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = RegExp(pattern);
  return (!regex.hasMatch(value)) ? false : true;
}

bool isFalsy(String val) => val == null || val.trim().isEmpty;

Future<T> platformValue<T>({
  T ios,
  T iosSimulator,
  T android,
  T androidSimulator,
}) async {
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  if (Platform.isAndroid) {
    try {
      // for some reason device info is not available in android emulator
      // use that to determine if its a real device.
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      if (androidInfo.isPhysicalDevice) {
        return android;
      } else {
        return androidSimulator ?? android;
      }
    } catch (e) {
      return androidSimulator ?? android;
    }
  } else if (Platform.isIOS) {
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    if (iosInfo.isPhysicalDevice) {
      return ios;
    } else {
      return iosSimulator ?? ios;
    }
  }
  return null;
}

int parseInt(String val) {
  if (val == null) return null;
  return int.tryParse(val);
}
