import 'package:flutter/material.dart';

class DismissKeyboardView extends StatelessWidget {
  final Widget child;

  DismissKeyboardView({@required this.child});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: child,
    );
  }
}
