import 'dart:async';
import 'package:bloc/bloc.dart';
import 'bloc.dart';

class RequestBloc extends Bloc<RequestEvent, RequestState> {
  @override
  RequestState get initialState => InitialRequestState();

  @override
  Stream<RequestState> mapEventToState(
    RequestEvent event,
  ) async* {
    if (event is CreateNewRequestEvent) {
      yield* _handleCreateNewRequestEvent(event);
    }
  }

  createNewRequest() {
    dispatch(CreateNewRequestEvent());
  }

  Stream<RequestState> _handleCreateNewRequestEvent(
    CreateNewRequestEvent event,
  ) async* {

  }
}
