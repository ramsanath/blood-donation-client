import 'package:equatable/equatable.dart';

abstract class RequestEvent extends Equatable {
  const RequestEvent();
}

class CreateNewRequestEvent extends RequestEvent {

  @override
  String toString() {
    return 'CreateNewRequestEvent{}';
  }
}