import 'package:equatable/equatable.dart';

abstract class RequestState extends Equatable {
  const RequestState();
}

class InitialRequestState extends RequestState {
  @override
  List<Object> get props => [];
}
