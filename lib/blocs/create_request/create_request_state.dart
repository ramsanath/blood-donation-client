import 'package:blood_donation/models/donation_request.dart';
import 'package:blood_donation/models/place.dart';

class CreateRequestState {
  final DonationRequest request;
  final bool userIsAttender;

  CreateRequestState({
    this.request,
    this.userIsAttender,
  });

  static initial() {
    return CreateRequestState(
      request: DonationRequest(
        venue: Place(),
        requiredImmediately: false,
      ),
      userIsAttender: true,
    );
  }

  CreateRequestState copyWith({
    DonationRequest request,
    bool userIsAttender,
  }) {
    return CreateRequestState(
      request: request ?? this.request,
      userIsAttender: userIsAttender ?? this.userIsAttender,
    );
  }
}
