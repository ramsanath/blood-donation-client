import 'package:blood_donation/models/location.dart';
import 'package:blood_donation/models/place.dart';
import 'package:equatable/equatable.dart';

abstract class CreateRequestEvent extends Equatable {
  const CreateRequestEvent();
}

class PatientNameChangeEvent extends CreateRequestEvent {
  final String payload;

  PatientNameChangeEvent(this.payload);
}

class PatientGenderChangeEvent extends CreateRequestEvent {
  final String payload;

  PatientGenderChangeEvent(this.payload);
}

class PatientBloodGroupChangeEvent extends CreateRequestEvent {
  final String payload;

  PatientBloodGroupChangeEvent(this.payload);
}

class UnitsRequiredChangeEvent extends CreateRequestEvent {
  final int payload;

  UnitsRequiredChangeEvent(this.payload);
}

class VenueNameChangeEvent extends CreateRequestEvent {
  final String payload;

  VenueNameChangeEvent(this.payload);
}

class VenueAddressChangeEvent extends CreateRequestEvent {
  final String payload;

  VenueAddressChangeEvent(this.payload);
}

class VenuePhoneNumberChangeEvent extends CreateRequestEvent {
  final String payload;

  VenuePhoneNumberChangeEvent(this.payload);
}

class VenueLocationChangeEvent extends CreateRequestEvent {
  final Location payload;

  VenueLocationChangeEvent(this.payload);
}

class VenueChangeEvent extends CreateRequestEvent {
  final Place payload;

  VenueChangeEvent(this.payload);
}

class RequiredOnChangeEvent extends CreateRequestEvent {
  final DateTime payload;

  RequiredOnChangeEvent(this.payload);
}

class RequiredImmediatelyChangeEvent extends CreateRequestEvent {
  final bool payload;

  RequiredImmediatelyChangeEvent(this.payload);
}

class DescriptionChangeEvent extends CreateRequestEvent {
  final String payload;

  DescriptionChangeEvent(this.payload);
}

class AttenderNameChangeEvent extends CreateRequestEvent {
  final String payload;

  AttenderNameChangeEvent(this.payload);
}

class AttenderPhoneNumberChangeEvent extends CreateRequestEvent {
  final String payload;

  AttenderPhoneNumberChangeEvent(this.payload);
}

class UserIsAttenderChangeEvent extends CreateRequestEvent {
  final bool payload;

  UserIsAttenderChangeEvent(this.payload);
}
