import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:blood_donation/data/request_repo.dart';
import 'package:blood_donation/data/user_repo.dart';
import 'package:blood_donation/models/donation_request.dart';
import 'package:blood_donation/models/place.dart';

import 'create_request_event.dart';
import 'create_request_state.dart';

class CreateRequestBloc extends Bloc<CreateRequestEvent, CreateRequestState> {
  final RequestRepo requestRepo;
  final UserRepo userRepo;

  CreateRequestBloc({this.requestRepo, this.userRepo});

  @override
  CreateRequestState get initialState => CreateRequestState.initial();

  DonationRequest get _request => currentState.request;

  Place get _venue => currentState.request.venue;

  CreateRequestState _updateRequest(DonationRequest request) {
    return currentState.copyWith(request: request);
  }

  CreateRequestState _updateVenue(Place venue) {
    return currentState.copyWith(request: _request.copyWith(venue: venue));
  }

  @override
  Stream<CreateRequestState> mapEventToState(CreateRequestEvent event) async* {
    if (event is PatientNameChangeEvent) {
      yield _updateRequest(_request.copyWith(patientName: event.payload));
    } else if (event is PatientGenderChangeEvent) {
      yield _updateRequest(_request.copyWith(patientGender: event.payload));
    } else if (event is PatientBloodGroupChangeEvent) {
      yield _updateRequest(_request.copyWith(patientBloodGroup: event.payload));
    } else if (event is UnitsRequiredChangeEvent) {
      yield _updateRequest(_request.copyWith(unitsRequired: event.payload));
    } else if (event is VenueNameChangeEvent) {
      yield _updateVenue(_venue.copyWith(name: event.payload));
    } else if (event is VenueAddressChangeEvent) {
      yield _updateVenue(_venue.copyWith(address: event.payload));
    } else if (event is VenuePhoneNumberChangeEvent) {
      yield _updateVenue(_venue.copyWith(phoneNumber: event.payload));
    } else if (event is VenueLocationChangeEvent) {
      yield _updateVenue(_venue.copyWith(location: event.payload));
    } else if (event is VenueChangeEvent) {
      yield _updateRequest(_request.copyWith(venue: event.payload));
    } else if (event is RequiredOnChangeEvent) {
      yield _updateRequest(_request.copyWith(requiredOn: event.payload));
    } else if (event is DescriptionChangeEvent) {
      yield _updateRequest(_request.copyWith(description: event.payload));
    } else if (event is AttenderNameChangeEvent) {
      yield _updateRequest(_request.copyWith(attenderName: event.payload));
    } else if (event is AttenderPhoneNumberChangeEvent) {
      yield _updateRequest(
        _request.copyWith(attenderPhoneNumber: event.payload),
      );
    } else if (event is RequiredImmediatelyChangeEvent) {
      yield _updateRequest(
        _request.copyWith(requiredImmediately: event.payload),
      );
    } else if (event is UserIsAttenderChangeEvent) {
      yield* _handleUserIsAttenderChangeEvent(event);
    }
  }

  Stream<CreateRequestState> _handleUserIsAttenderChangeEvent(
    UserIsAttenderChangeEvent event,
  ) async* {
//    yield currentState.copyWith(userIsAttender: event.payload);
    final user = userRepo.currentUser;
    yield currentState.copyWith(
      request: _request.copyWith(
        attenderName: event.payload ? user.name : '',
        attenderPhoneNumber: event.payload ? user.phoneNumber : '',
      ),
      userIsAttender: event.payload,
    );
  }
}
