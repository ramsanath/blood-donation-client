import 'package:blood_donation/models/user_model.dart';
import 'package:equatable/equatable.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
}

class AttemptRegisterEvent extends RegisterEvent {
  final User user;

  AttemptRegisterEvent({this.user});

  @override
  String toString() {
    return 'AttemptRegisterEvent{user: $user}';
  }
}

class RegistrationFailedEvent extends RegisterEvent {
  final String message;

  RegistrationFailedEvent({this.message});

  @override
  String toString() {
    return 'RegistrationFailedEvent{message: $message}';
  }
}
