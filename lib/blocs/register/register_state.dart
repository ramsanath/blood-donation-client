import 'package:blood_donation/models/user_model.dart';

class RegisterState {
  final User user;
  final bool isLoading;
  final String error;
  final bool registerSuccess;

  RegisterState({
    this.user,
    this.isLoading,
    this.error,
    this.registerSuccess,
  });

  factory RegisterState.initial() {
    return RegisterState(
      user: User(),
      isLoading: false,
      error: null,
      registerSuccess: null,
    );
  }

  RegisterState copyWith({
    User user,
    bool isLoading,
    String error,
    bool registerSuccess,
  }) {
    return RegisterState(
      user: user ?? this.user,
      isLoading: isLoading ?? this.isLoading,
      error: error ?? this.error,
      registerSuccess: registerSuccess ?? this.registerSuccess,
    );
  }

  @override
  String toString() {
    return 'RegisterState{user: $user, isLoading: $isLoading, error: $error, registerSuccess: $registerSuccess}';
  }
}
