import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:blood_donation/blocs/auth/bloc.dart';
import 'package:blood_donation/data/user_repo.dart';
import 'package:blood_donation/models/user_model.dart';

import 'register_event.dart';
import 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final UserRepo userRepo;
  final AuthBloc authBloc;

  RegisterBloc({this.userRepo, this.authBloc});

  @override
  RegisterState get initialState => RegisterState.initial();

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is AttemptRegisterEvent) {
      yield* _handleAttemptRegisterEvent(event);
    }
    if (event is RegistrationFailedEvent) {
      yield* _handleRegistrationFailedEvent(event);
    }
  }

  Stream<RegisterState> _handleAttemptRegisterEvent(
    AttemptRegisterEvent event,
  ) async* {
    yield currentState.copyWith(isLoading: true, error: null);

    try {
      User loginResponse = await userRepo.register(event.user);
      authBloc.login(loginResponse);
      yield currentState.copyWith(registerSuccess: true);
    } catch (e) {
      onRegisterFailed(e);
    }
  }

  Stream<RegisterState> _handleRegistrationFailedEvent(
    RegistrationFailedEvent event,
  ) async* {
    yield currentState.copyWith(isLoading: false, error: event.message);
  }

  void register(User user) {
    dispatch(AttemptRegisterEvent(user: user));
  }

  void onRegisterFailed(String message) {
    dispatch(RegistrationFailedEvent(message: message));
  }
}
