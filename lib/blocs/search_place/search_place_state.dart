import 'package:blood_donation/models/place.dart';
import 'package:blood_donation/models/place_suggestion.dart';

class SearchPlaceState {
  final List<PlaceSuggestion> suggestions;
  final Place place;
  final bool loadingSuggestions;
  final bool loadingDetail;
  final bool error;

  SearchPlaceState({
    this.suggestions,
    this.place,
    this.loadingSuggestions,
    this.loadingDetail,
    this.error,
  });

  bool get loading => loadingDetail || loadingSuggestions;

  static SearchPlaceState initial() => SearchPlaceState(
        place: null,
        suggestions: [],
        error: false,
        loadingDetail: false,
        loadingSuggestions: false,
      );

  SearchPlaceState copyWith({
    List<PlaceSuggestion> suggestions,
    Place place,
    bool loadingSuggestions,
    bool loadingDetail,
    bool error,
  }) {
    return new SearchPlaceState(
      suggestions: suggestions ?? this.suggestions,
      place: place ?? this.place,
      loadingSuggestions: loadingSuggestions ?? this.loadingSuggestions,
      loadingDetail: loadingDetail ?? this.loadingDetail,
      error: error ?? this.error,
    );
  }
}
