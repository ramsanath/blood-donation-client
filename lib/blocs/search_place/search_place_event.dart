import 'package:blood_donation/models/place.dart';
import 'package:blood_donation/models/place_suggestion.dart';
import 'package:equatable/equatable.dart';

abstract class SearchPlaceEvent extends Equatable {
  const SearchPlaceEvent();
}

class SearchForPlaceEvent extends SearchPlaceEvent {
  final String payload;

  SearchForPlaceEvent(this.payload);
}

class SearchForPlaceSuccessEvent extends SearchPlaceEvent {
  final List<PlaceSuggestion> payload;

  SearchForPlaceSuccessEvent(this.payload);
}

class GetPlaceDetailEvent extends SearchPlaceEvent {
  final String placeId;

  GetPlaceDetailEvent(this.placeId);
}

class GetPlaceDetailSuccessEvent extends SearchPlaceEvent {
  final Place place;

  GetPlaceDetailSuccessEvent(this.place);
}
