import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:blood_donation/data/hospital_repo.dart';
import 'package:blood_donation/models/place.dart';
import 'package:blood_donation/models/place_suggestion.dart';
import 'package:flutter/foundation.dart';
import 'package:geolocator/geolocator.dart';

import 'search_place_event.dart';
import 'search_place_state.dart';

class SearchPlaceBloc extends Bloc<SearchPlaceEvent, SearchPlaceState> {
  final PlaceRepo placeRepo;

  SearchPlaceBloc({@required this.placeRepo});

  @override
  SearchPlaceState get initialState => SearchPlaceState.initial();

  search(String name) {
    dispatch(SearchForPlaceEvent(name));
  }

  getPlaceDetail(PlaceSuggestion placeSuggestion) {
    if (placeSuggestion.placeId == null) {
      final place = Place(name: placeSuggestion.mainText);
      dispatch(GetPlaceDetailSuccessEvent(place));
    } else {
      dispatch(GetPlaceDetailEvent(placeSuggestion.placeId));
    }
  }

  @override
  Stream<SearchPlaceState> mapEventToState(
    SearchPlaceEvent event,
  ) async* {
    if (event is SearchForPlaceEvent) {
      yield* _handleSearchForPlaceEvent(event);
    } else if (event is GetPlaceDetailEvent) {
      yield* _handleGetPlaceDetailEvent(event);
    } else if (event is SearchForPlaceSuccessEvent) {
      yield* _handleSearchForPlaceSuccessEvent(event);
    } else if (event is GetPlaceDetailSuccessEvent) {
      yield* _handleGetPlaceDetailSuccessEvent(event);
    }
  }

  Stream<SearchPlaceState> _handleSearchForPlaceEvent(
    SearchForPlaceEvent event,
  ) async* {
    yield currentState.copyWith(loadingSuggestions: true, error: false);
    try {
      final location = await _getLocation();
      final results = await placeRepo.searchPlaces(event.payload, location);
      // add what the user typed as a suggestion so that they
      // can select it if there was no result.
      results.add(PlaceSuggestion(mainText: event.payload));
      dispatch(SearchForPlaceSuccessEvent(results));
    } catch (e) {
      yield currentState.copyWith(loadingSuggestions: false, error: true);
    }
  }

  Stream<SearchPlaceState> _handleGetPlaceDetailEvent(
    GetPlaceDetailEvent event,
  ) async* {
    yield currentState.copyWith(
      loadingDetail: true,
      error: false,
    );

    try {
      final place = await placeRepo.placeDetails(event.placeId);
      dispatch(GetPlaceDetailSuccessEvent(place));
    } catch (e) {
      yield currentState.copyWith(loadingDetail: false, error: true);
    }
  }

  Stream<SearchPlaceState> _handleSearchForPlaceSuccessEvent(
    SearchForPlaceSuccessEvent event,
  ) async* {
    yield currentState.copyWith(
      loadingSuggestions: false,
      error: false,
      suggestions: event.payload,
    );
  }

  Stream<SearchPlaceState> _handleGetPlaceDetailSuccessEvent(
    GetPlaceDetailSuccessEvent event,
  ) async* {
    yield currentState.copyWith(
      loadingDetail: false,
      error: false,
      place: event.place,
    );
  }

  Future<String> _getLocation() async {
//    Map<PermissionGroup, PermissionStatus> permissions =
//        await PermissionHandler()
//            .requestPermissions([PermissionGroup.location]);

    Position position = await Geolocator().getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );
    return "${position.latitude},${position.longitude}";
  }
}
