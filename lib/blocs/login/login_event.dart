import 'package:blood_donation/models/login_credentials.dart';
import 'package:equatable/equatable.dart';

abstract class LoginEvent extends Equatable {
  LoginEvent([List props = const []]) : super(props);
}

class AttemptLoginEvent extends LoginEvent {
  final LoginCredentials data;

  AttemptLoginEvent(this.data);

  @override
  String toString() {
    return 'AttemptLoginEvent{data: $data}';
  }


}

class LoginErrorEvent extends LoginEvent {
  final String error;

  LoginErrorEvent({this.error});

  @override
  String toString() {
    return 'LoginErrorEvent{error: $error}';
  }
}
