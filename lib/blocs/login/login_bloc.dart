import 'package:bloc/bloc.dart';
import 'package:blood_donation/blocs/auth/bloc.dart';
import 'package:blood_donation/data/user_repo.dart';
import 'package:blood_donation/models/login_credentials.dart';
import 'package:blood_donation/models/user_model.dart';
import 'package:meta/meta.dart';

import 'login_event.dart';
import 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepo userRepo;
  final AuthBloc authBloc;

  LoginBloc({
    @required this.userRepo,
    @required this.authBloc,
  })  : assert(userRepo != null),
        assert(authBloc != null);

  @override
  LoginState get initialState => InitialLoginState();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is AttemptLoginEvent) {
      yield* _handleAttemptLogin(event);
    } else if (event is LoginErrorEvent) {
      yield LoginErrorState(error: event.error);
    }
  }

  Stream<LoginState> _handleAttemptLogin(AttemptLoginEvent event) async* {
    yield LoginLoadingState();
    try {
      User user = await userRepo.login(event.data);
      authBloc.login(user);
      yield LoginSuccessState(user: user);
    } catch (e) {
      yield LoginErrorState(error: e.toString());
    }
  }

  login(LoginCredentials data) async {
    dispatch(AttemptLoginEvent(data));
  }

  onLoginError(String message) {
    dispatch(LoginErrorEvent(error: message));
  }
}
