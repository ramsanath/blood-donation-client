import 'package:blood_donation/models/user_model.dart';
import 'package:equatable/equatable.dart';

class LoginState extends Equatable {
  LoginState([List props = const []]) : super(props);
}

class InitialLoginState extends LoginState {
  @override
  String toString() {
    return 'InitialLoginState{}';
  }
}

class LoginLoadingState extends LoginState {
  @override
  String toString() {
    return 'LoginLoadingState{}';
  }
}

class LoginErrorState extends LoginState {
  final String error;

  LoginErrorState({this.error});

  @override
  String toString() {
    return 'LoginErrorState{error: $error}';
  }
}

class LoginSuccessState extends LoginState {
  final User user;

  LoginSuccessState({this.user});

  @override
  String toString() {
    return 'LoginSuccessState{user: $user}';
  }
}
