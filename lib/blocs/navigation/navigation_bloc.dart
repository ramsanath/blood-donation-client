import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import './bloc.dart';

class NavigationBloc extends Bloc<NavigationEvent, dynamic> {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  NavigatorState get navigator => navigatorKey.currentState;

  @override
  dynamic get initialState => null;

  @override
  Stream<dynamic> mapEventToState(NavigationEvent event) async* {
    if (event is LoggedInNavigationEvent) {
      navigator.pushNamedAndRemoveUntil('/blocs.home', (route) => false);
    } else if (event is LoggedOutNavigationEvent) {
      navigator.pushNamedAndRemoveUntil('/ui.welcome', (route) => false);
    } else if (event is CreateRequestNavigationEvent) {
      navigator.pushNamed('/create_request');
    }
  }

  goToHome() {
    dispatch(LoggedInNavigationEvent());
  }

  goToWelcome() {
    dispatch(LoggedOutNavigationEvent());
  }

  goToCreateRequest() {
    dispatch(CreateRequestNavigationEvent());
  }
}

