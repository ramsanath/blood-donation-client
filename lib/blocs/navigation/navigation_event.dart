import 'package:equatable/equatable.dart';

abstract class NavigationEvent extends Equatable {
  const NavigationEvent();
}

class PopNavigationEvent extends NavigationEvent {
  @override
  String toString() {
    return 'PopNavigationEvent{}';
  }
}

class LoggedOutNavigationEvent extends NavigationEvent {
  @override
  String toString() {
    return 'LoggedOutNavigationEvent{}';
  }
}

class LoggedInNavigationEvent extends NavigationEvent {
  @override
  String toString() {
    return 'LoggedInNavigationEvent{}';
  }
}

class CreateRequestNavigationEvent extends NavigationEvent {
  @override
  String toString() {
    return 'NewRequestNavigationEvent{}';
  }
}
