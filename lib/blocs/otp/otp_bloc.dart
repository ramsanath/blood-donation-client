import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:blood_donation/data/api.dart';

import 'otp_event.dart';
import 'otp_state.dart';

class OtpBloc extends Bloc<OtpEvent, OtpState> {
  final Api api;

  OtpBloc({@required this.api});

  @override
  OtpState get initialState => OtpInitialState();

  @override
  Stream<OtpState> mapEventToState(OtpEvent event) async* {
    if (event is SendOtpEvent) {
      yield* _handleSendOtp(event);
    }
  }

  void sendOtp(String phoneNumber) {
    dispatch(SendOtpEvent(phoneNumber: phoneNumber));
  }

  Stream<OtpState> _handleSendOtp(SendOtpEvent event) async* {
    yield OtpSendingState();
    try {
      await api.sendOtp(event.phoneNumber);
      yield OtpSentState();
    } catch (e) {
      yield OtpSendFailState(error: e);
    }
  }

}
