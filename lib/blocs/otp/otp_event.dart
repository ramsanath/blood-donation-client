import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

class OtpEvent extends Equatable {
  OtpEvent([List props = const []]) : super(props);
}

class SendOtpEvent extends OtpEvent {
  final String phoneNumber;

  SendOtpEvent({@required this.phoneNumber});
}