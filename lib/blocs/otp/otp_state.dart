import 'package:equatable/equatable.dart';

class OtpState extends Equatable {
  OtpState([List props = const []]) : super(props);
}

class OtpInitialState extends OtpState {
  @override
  String toString() {
    return 'OtpInitialState{}';
  }
}

class OtpSendingState extends OtpState {
  @override
  String toString() {
    return 'OtpSendingState{}';
  }
}

class OtpSentState extends OtpState {
  @override
  String toString() {
    return 'OtpSentState{}';
  }
}

class OtpSendFailState extends OtpState {
  final String error;

  OtpSendFailState({this.error});

  @override
  String toString() {
    return 'OtpSendFailState{error: $error}';
  }
}
