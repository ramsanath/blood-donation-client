import 'package:blood_donation/models/user_model.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AuthEvent extends Equatable {
  AuthEvent([List props = const []]) : super(props);
}

class AppStartedEvent extends AuthEvent {
  @override
  String toString() => 'AppStarted';
}

class LoggedInEvent extends AuthEvent {
  final User user;

  LoggedInEvent({@required this.user}) : super([user]);

  @override
  String toString() {
    return 'LoggedInEvent{user: $user}';
  }
}

class LoggedOutEvent extends AuthEvent {
  @override
  String toString() => 'LoggedOut';
}
