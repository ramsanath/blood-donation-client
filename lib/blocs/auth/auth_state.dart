import 'package:equatable/equatable.dart';

abstract class AuthState extends Equatable {
  AuthState([List props = const []]) : super(props);
}

class AuthUninitializedState extends AuthState {
  @override
  String toString() {
    return 'AuthUninitializedState{}';
  }
}

class AuthAuthenticatedState extends AuthState {
  @override
  String toString() {
    return 'AuthAuthenticatedState{}';
  }
}

class AuthUnauthenticatedState extends AuthState {
  @override
  String toString() {
    return 'AuthUnauthenticatedState{}';
  }
}
