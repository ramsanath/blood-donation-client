import 'package:bloc/bloc.dart';
import 'package:blood_donation/blocs/navigation/bloc.dart';
import 'package:blood_donation/data/user_repo.dart';
import 'package:blood_donation/models/user_model.dart';
import 'package:meta/meta.dart';

import 'auth_event.dart';
import 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final UserRepo userRepo;
  final NavigationBloc navigationBloc;

  AuthBloc({
    @required this.userRepo,
    @required this.navigationBloc,
  }) : assert(userRepo != null);

  @override
  AuthState get initialState => AuthUninitializedState();

  logout() {
    dispatch(LoggedOutEvent());
  }

  login(User user) {
    dispatch(LoggedInEvent(user: user));
  }

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is AppStartedEvent) {
      yield* _handleAppStarted(event);
    } else if (event is LoggedInEvent) {
      yield* _handleLoggedIn(event);
    } else if (event is LoggedOutEvent) {
      yield* _handleLoggedOut(event);
    }
  }

  Stream<AuthState> _handleAppStarted(AppStartedEvent event) async* {
    final bool hasToken = await userRepo.isLoggedIn();
    if (hasToken) {
      yield AuthAuthenticatedState();
    } else {
      yield AuthUnauthenticatedState();
    }
  }

  Stream<AuthState> _handleLoggedIn(LoggedInEvent event) async* {
    await userRepo.persistUserInfo(event.user);
    navigationBloc.goToHome();
    yield AuthAuthenticatedState();
  }

  Stream<AuthState> _handleLoggedOut(LoggedOutEvent event) async* {
    await userRepo.deleteToken();
    navigationBloc.goToWelcome();
    yield AuthUnauthenticatedState();
  }
}
